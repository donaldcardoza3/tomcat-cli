#!/bin/bash
TOMCAT_URL="https://dlcdn.apache.org/tomcat/tomcat-10/v10.1.8/bin/apache-tomcat-10.1.8.tar.gz"



function check_java_home {
    if [ -z ${JAVA_HOME} ]
    then
        echo 'Could not find JAVA_HOME. Please install Java and set JAVA_HOME'
	exit
    else 
	echo 'JAVA_HOME found: '$JAVA_HOME
        if [ ! -e ${JAVA_HOME} ]
        then
	    echo 'Invalid JAVA_HOME. Make sure your JAVA_HOME path exists'
	    exit
        fi
    fi
}



echo 'checking java and tomcat if it already exists..'
check_java_home

if test $(find /opt/ -type d -name "tomcat*" | wc -c) -eq 0
then
    echo "Tomact not installed"
    
 else
  echo "Tomcat already installed"
  exit
fi


echo 'Downloading tomcat..'
if [ ! -f /etc/apache-tomcat-10*tar.gz ]
then
    curl -O $TOMCAT_URL
fi
echo 'Finished downloading...'

echo 'Creating install directories...'
sudo mkdir -p '/opt/tomcat/10'

if [ -d "/opt/tomcat/10" ]
then
    echo 'Extracting binaries to install directory...'
    sudo tar xzf apache-tomcat-10*tar.gz -C "/opt/tomcat/10" --strip-components=1
    echo 'Creating tomcat user group...'
    sudo groupadd ps-kuwait
    sudo useradd -s /bin/false -g ps-kuwait -d /opt/tomcat ps-kuwait
    
    echo 'Setting file permissions...'
    cd "/opt/tomcat/10"
    sudo chgrp -R ps-kuwait "/opt/tomcat/10"
    sudo chmod -R g+rw conf
    sudo chmod -R g+x conf

  
    sudo chmod -R g+w conf

    sudo chown -R ps-kuwait webapps/ work/ temp/ logs/

    echo 'Setting up tomcat service...'
    sudo touch tomcat.service
    sudo chmod 777 tomcat.service 
    echo "[Unit]" > tomcat.service
    echo "Description=Apache Tomcat Web Application Container" >> tomcat.service
    echo "After=network.target" >> tomcat.service

    echo "[Service]" >> tomcat.service
    echo "Type=forking" >> tomcat.service

    echo "Environment=JAVA_HOME=$JAVA_HOME" >> tomcat.service
    echo "Environment=CATALINA_PID=/opt/tomcat/10/temp/tomcat.pid" >> tomcat.service
    echo "Environment=CATALINA_HOME=/opt/tomcat/10" >> tomcat.service
    echo "Environment=CATALINA_BASE=/opt/tomcat/10" >> tomcat.service
    echo "Environment=CATALINA_OPTS=-Xms512M -Xmx1024M -server -XX:+UseParallelGC" >> tomcat.service
    echo "Environment=JAVA_OPTS=-Djava.awt.headless=true -Djava.security.egd=file:/dev/./urandom" >> tomcat.service

    echo "ExecStart=/opt/tomcat/10/bin/startup.sh" >> tomcat.service
    echo "ExecStop=/opt/tomcat/10/bin/shutdown.sh" >> tomcat.service

    echo "User=ps-kuwait" >> tomcat.service
    echo "Group=ps-kuwait" >> tomcat.service
    echo "UMask=0007" >> tomcat.service
    echo "RestartSec=10" >> tomcat.service
    echo "Restart=always" >> tomcat.service

    echo "[Install]" >> tomcat.service
    echo "WantedBy=multi-user.target" >> tomcat.service

    sudo mv tomcat.service /etc/systemd/system/tomcat.service
    sudo chmod 755 /etc/systemd/system/tomcat.service
    sudo systemctl daemon-reload
    
    echo 'Starting tomcat server....'
    sudo systemctl start tomcat
    exit
else
    echo 'Could not locate installation direcotry..exiting..'
    exit
fi
fi
